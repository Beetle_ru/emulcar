CMPL=g++
INCLUDE=include
SRC=src
OBJ=obj
BIN=bin
LIBDIR=/usr/local/lib/:/usr/lib/i386-linux-gnu/


build: $(OBJ)/emulcar.o mkdirbin
	$(CMPL)  $(OBJ)/emulcar.o -L${LIBDIR} -lsimavr -lelf -lpthread -o  $(BIN)/emulcar

$(OBJ)/%.o: $(SRC)/%.cpp mkdirobj
	$(CMPL) -c $< -o $@ -I$(INCLUDE)

avr: mkdirbin
	$(MAKE) -C avr

mkdirobj:
	mkdir -p $(OBJ)

mkdirlib:
	mkdir -p $(LIB)
  
mkdirbin:
	mkdir -p $(BIN)

clean:
	rm -R $(OBJ) $(LIB) $(BIN)
