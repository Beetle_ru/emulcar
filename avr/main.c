#include <avr/io.h>
#include <avr/interrupt.h>

#include <stdio.h>
#include <util/delay.h>

#include "avr_mcu_section.h"
AVR_MCU(F_CPU, "atmega128"); // for emulator


ISR( TIMER1_OVF_vect ) {
	//PORTB != PORTB;
	if (PORTB == 0x00) PORTB = 0xff;
	else PORTB = 0x00;
	if (PORTA < 0xff)
		PORTA++;
	else
		PORTA=0x00;
}
 
int main() {
	DDRA=0xff;
	DDRB=0xff;
	PORTB = 0x00;
	PORTA = 0x00;
	//DDRB = ( 1 << PB0 );  // настраиваем PB0 на выход
	TCCR1B = (0<<CS12)|(1<<CS11)|(1<<CS10); // настраиваем делитель
	TIMSK |= (1<<TOIE1); // разрешаем прерывание по переполнению таймера
	TCNT1 = 64456;        // выставляем начальное значение TCNT1
	sei();                // выставляем бит общего разрешения прерываний
	while(1);             // вечный цикл
	return 0;
}
