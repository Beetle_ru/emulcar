#ifndef __ENULCARH__
#define __ENULCARH__
#include <simavr/sim_avr.h>
#include <simavr/avr_twi.h>
#include <simavr/sim_elf.h>
#include <simavr/sim_gdb.h>
#include <simavr/sim_vcd_file.h>
#include <simavr/avr_ioport.h>

#include <string.h>
#include <pthread.h>
#include <stdlib.h>



#endif // __ENULCARH__
