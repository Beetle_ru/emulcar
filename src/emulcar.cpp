#include "emulcar.h"

avr_t * avr = NULL;
avr_vcd_t vcd_file;

// for print values
const int portCnt = 5;
char portsVal[portCnt];
char portsPrfx[] = {'A', 'B', 'C', 'D', 'E'};
enum Ports { PORTA, PORTB, PORTC, PORTD, PORTE};

void printPorts(char *pvp, int cnt, char *prefix) {
	for (int porti = 0; porti < cnt; porti++) {
		char value = pvp[porti];
		printf("%c[", prefix[porti]);
		for (int i = 0; i < 8; i++) {
			if (value & 1)
				printf("#");
			else
				printf("-");

			value >>= 1;
		}
		printf("] ");
	}
	printf("\n");
}

// port change handler
void portChanged(struct avr_irq_t * irq, uint32_t value, void * param) {
	portsVal[*((Ports*)param)] = value;
	printPorts(portsVal, portCnt, portsPrfx);
}

static void * avrRunThread(void * param) {
	while (1) {
		avr_run(avr);
	}
	return NULL;
}

int main(int argc, char *argv[]) {
	elf_firmware_t f;
	const char * fname =  "bin/main.elf";
	
	printf("Firmware pathname is %s\n", fname);
	elf_read_firmware(fname, &f);

	printf("firmware %s f=%d mmcu=%s\n", fname, (int)f.frequency, f.mmcu);

	avr = avr_make_mcu_by_name(f.mmcu);
	if (!avr) {
		fprintf(stderr, "%s: AVR '%s' not known\n", argv[0], f.mmcu);
		return 1;
	}

	avr_init(avr);
	avr_load_firmware(avr, &f);

	

	// set vcd loger
	avr_vcd_init(avr, "bin/gtkwave_output.vcd", &vcd_file, 100000 /* usec */);
	
	for (int i = 0; i < portCnt; i++) {
	char *name = (char*)malloc(sizeof(char) * 5);
	Ports *port = (Ports*)malloc(sizeof(Ports));
	
	sprintf(name, "PORT%c", portsPrfx[i]);
	*port = (Ports)i;
	
	// port changed subscribe
	avr_irq_register_notify(
		avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ(portsPrfx[i]), IOPORT_IRQ_PIN_ALL),
		portChanged, 
		port
	);
	
	// port changed subscribe for vcd
	avr_vcd_add_signal(
			&vcd_file, 
			avr_io_getirq(avr, AVR_IOCTL_IOPORT_GETIRQ(portsPrfx[i]), IOPORT_IRQ_PIN_ALL), 
			8 /* bits */ ,
			name 
		);
	}
	
	avr_vcd_start(&vcd_file);
	
	pthread_t run;
	pthread_create(&run, NULL, avrRunThread, NULL);
	
	printf("press \"Enter\" for exit\n");
	getchar();
	
	avr_vcd_stop(&vcd_file);
	
	
	return 0;
}

